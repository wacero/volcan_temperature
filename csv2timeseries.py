from influxdb import InfluxDBClient
import logging
import json
import configparser
from datetime import datetime
import os,fnmatch




def read_config_file(json_file_path):
    '''
    Read a json file and returns a python dict
    
    :param string json_file_path: path to the configuration file
    :returns: dict: dictionary with the server parameters
    '''
    
    json_file=check_file(json_file_path)
    with open(json_file) as json_data:
        return json.load(json_data)
    
def read_parameters(file_path):
    """
    Read a configuration text file
    
    :param string file_path: path to configuration text file
    :returns: dict: dict of a parser object
    """
    parameter_file=check_file(file_path)
    parser=configparser.ConfigParser()
    parser.read(parameter_file)    
    return parser._sections



def check_file(file_path):
    '''
    Check if the file exists
    
    :param string file_path: path to file to check
    :return: file_path
    :raises Exception e: General exception if file doesn't exist. 
    '''
    try:
        
        with open(file_path):
            return file_path

    except Exception as e:
        logging.error("Error in check_file(%s). Error: %s " %(file_path,str(e)))
        raise Exception("Error in check_file(%s). Error: %s " %(file_path,str(e)))



def get_temperature_files(temperature_path,file_extension):
    
    """
    Return irp files found in a folder as a list
    """
    irp_files_list=[]
    for dir_path,dir_names, file_names in os.walk(temperature_path):
        
        for temperature_file in file_names:
            if fnmatch.fnmatch(temperature_file,"*%s*" %file_extension):
                irp_files_list.append(os.path.join(temperature_path,temperature_file))
    
    return irp_files_list
                

def preprocess_temperature_date(temperature_text):
    
    temperature,unixtime=temperature_text.split("\t")[:2]
    temperature=temperature.replace(",",".")
    unixtime=unixtime.replace(",",".")
    #utc_datetime=UTCDateTime(datetime.utcfromtimestamp(float(unixtime)))
    utc_datetime=datetime.utcfromtimestamp(float(unixtime))
    return temperature,utc_datetime



def insert_temp_2_influxdb(client_ifxdb, db_name,site,temp_type, temperature_list):
    
    data_list=[]
    
    for data in temperature_list[:10]:
        data_list.append("%s,type='%s' temperature=%s %s" %(site,temp_type, data['temperature'], data['time']))
    
    result=client_ifxdb.write_points(data_list,time_precision="s",database=db_name,protocol='line')
    #result=client_ifxdb.write("%s,type='%s' temperature=%s %s"
    #                               %(site,temp_type, temperature, time.ns),{'db':'%s'%db_name,'time_precision':'ms'},protocol='line')


    logging.info(result)
    
    
