from influxdb import InfluxDBClient
import logging, logging.config
import sys
import csv2timeseries


logging.config.fileConfig('./config/logging.ini', disable_existing_loggers=False)
logger=logging.getLogger('volcan_temperature')


def main():
    
    is_error=False
    
    if len(sys.argv)==1:
        is_error=True
    
    else:
        try:
            run_param=csv2timeseries.read_parameters(sys.argv[1])
            
        except Exception as e:
            logger.info("Error reading configuration in file: %s" %str(e))
            raise Exception("Error reading configuration in file: %s" %str(e))

        try:
            '''
            Read the configuration text file with information of data servers, database servers, and stations to process:
            '''
            
            tsdb_server_id=run_param["TIMESERIES_DATABASE"]["id"]
            tsdb_config_file=run_param["TIMESERIES_DATABASE"]["file_path"]
            
            temperature_files_folder=run_param['TEMPERATURE']['folder_path']
            file_extension= run_param['TEMPERATURE']['file_extension']
            site=run_param['TEMPERATURE']['site']
            temperature_type=run_param['TEMPERATURE']['temperature_type']
                          
        except Exception as e:
            logger.error("Error getting parameters: %s" %str(e))
            raise Exception("Error getting parameters: %s" %str(e))

        try:
            '''
            Read JSON files with information of the data servers
            and get that info
            '''
            tsdb_param=csv2timeseries.read_config_file(tsdb_config_file)
            tsdb_name=tsdb_param[tsdb_server_id]['dbname']
                        
        except Exception as e:
            logger.error("Error reading configuration file: %s" %str(e))
            raise Exception("Error reading configuration file: %s" %str(e))

        try: 
            tsdb_client=InfluxDBClient(host=tsdb_param[tsdb_server_id]['ip'],port=tsdb_param[tsdb_server_id]['port'],
                                       username=tsdb_param[tsdb_server_id]['user'],password=tsdb_param[tsdb_server_id]['password'],
                                       database=tsdb_param[tsdb_server_id]['dbname'])
        except Exception as e:
            logger.error("Failed to create database client: %s" %str(e))
            raise Exception("Failed to create database client: %s" %str(e))

    
        irp_list=csv2timeseries.get_temperature_files(temperature_files_folder, file_extension)
    
        print(irp_list)
        
        for file in irp_list:
            logger.info("Reading %s file" %file)
            data=open(file,"r")
            
            try:
                data_lines=data.readlines()
                data_lines_list=[]
                for line in data_lines[1:]:
                
                    temperature,temperature_datetime=csv2timeseries.preprocess_temperature_date(line)
                    data_lines_list.append({'temperature':temperature,'time':int(temperature_datetime.timestamp())})
                    #print(temperature,temperature_datetime)
                    
                csv2timeseries.insert_temp_2_influxdb(tsdb_client, tsdb_name, site, temperature_type, data_lines_list)
   
            
            except Exception as e:
                logger.error("Error reading temperature file. Error was: %s in %s" %(str(e),file))
            

    
    if is_error:
        logger.info(f'Usage: python {sys.argv[0]} configuration_file.txt start_date end_date')
        print(f'Usage: python {sys.argv[0]} CONFIGURATION_FILE.txt start_date end_date ') 
        
        
        
main()
