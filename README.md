Código en python para enviar  archivos IRP a una base de series de tiempo y poder visualizar con Grafana.

## Instalación de librerías en conda

```bash
$ conda create -n volcan_temperature
$ conda activate volcan_temperature
$ conda install python=3.8 influxdb pygelf
```

## Descargar el programa 

```bash
$ git clone https://gitlab.com/wacero/volcan_temperature.git

```

## Ejecutar el programa 

```bash

$ cd volcan_temperature 
$ python run_csv2timeseries.py ./configuracion.txt 
```

## Archivo de configuración 
